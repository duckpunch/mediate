## Start

Copy `mediate.yaml.example` to `mediate.yaml`.

# Get things up in dev

```
docker-compose up
```

## File structure

```
images/
    <uploaded_media.id:uuid>/
        small.jpg
        xsmall.jpg
        <escaped orig name>.<uploaded_media.extension>
videos/
    <uploaded_media.id:uuid>/
        small.jpg
        xsmall.jpg
        <escaped orig name>.<ext>
        transcoded.mp4
        subtitles.vtt
```

## New pathing

* `media`
    * `GET /`
    * `GET /<id>`
    * `DELETE /<id>`
    * `POST /`
    * `POST /<id>/subtitles`
* `users`
    * `GET /`
    * `DELETE /`
    * `POST /`
* `oidc`
    * `providers`
    * `session`
    * `callback`
    * `login`
        * `?provider=<provider-id>`

## Notes

- db shell
    - `docker run -it --rm --link database:postgres --net mediate_default postgres psql -h mediate_database_1 -U mediate`
    - network comes from `docker network ls`
- `docker-compose restart` on file change
- `docker-compose rm` kills anonymous volumes
- how to generate a migration
    - run container
    - log into container, generate migration

## OAuth2

1. Send a request from client to IDP: `https://gitlab.com/oauth/authorize?response_type=code&client_id=<id>&redirect_uri=<callback>&state=<anti-csrf>`
    * This is the GRANT part.
2. Get called back on the callback with the grant, proceed to get the token using configured method.

[More thorough explanation](https://docs.authlib.org/en/latest/client/oauth2.html#oauth2session-for-authorization-code).

## ffmpeg guide

Snag a screenshot.

```
ffmpeg -i <video> -vcodec mjpeg -vframes 1 -f image2 /tmp/pic.jpg
```

Convert to mp4.

```
ffmpeg -i <video> -vcodec copy -acodec copy /tmp/out.mp4
```
