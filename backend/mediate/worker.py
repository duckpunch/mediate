import os

import redis
from rq import Worker, Queue, Connection

listen = ['default']

redis_url = os.getenv('REDIS_URL')

connection = redis.from_url(redis_url)

if __name__ == '__main__':
    with Connection(connection):
        # Not perfect because it kills current jobs in progress
        for zombie in Worker.all():
            zombie.register_death()

        worker = Worker([Queue('default')])
        print(worker.work())
