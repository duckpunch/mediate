import uuid

import flask
import werkzeug
import voluptuous

from mediate.auth import admin_required
from mediate.persistence import tables


users = flask.Blueprint('resources', __name__)


@users.route('/')
@admin_required
def get_users():
    with tables.get_connection() as connection:
        results = connection.execute(tables.authorized_users.select())

    return flask.jsonify([
        {
            'userinfo_value': row['userinfo_value'],
            'provider': row['provider'],
            'id': row['id'],
        }
        for row in map(lambda raw: dict(raw.items()), results)
    ])


@users.route('/<user_id>', methods=['DELETE'])
@admin_required
def delete_users(user_id):
    with tables.get_connection() as connection:
        connection.execute(
            tables.authorized_users.delete().where(
                tables.authorized_users.c.id == user_id,
            )
        )
    return ''


@users.route('/', methods=['POST'])
@admin_required
def create_users():
    try:
        # force=True ignores the mimetype
        new_user = flask.request.get_json(force=True)
    except werkzeug.exceptions.BadRequest:
        return '', 400

    voluptuous.Schema({
        voluptuous.Required('userinfo_value'): str,
        voluptuous.Required('provider'): str,
    })(new_user)

    with tables.get_connection() as connection:
        connection.execute(tables.authorized_users.insert().values(
            id=str(uuid.uuid4()), **new_user
        ))

    return '', 201
