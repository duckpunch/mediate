import os
import uuid
from datetime import datetime

import dateutil.parser
import flask
from werkzeug.utils import secure_filename

from mediate import constants
from mediate.auth import admin_required
from mediate.images import save_rotated_image, get_image_datetime
from mediate.persistence import tables
from mediate.transcode import transcode_video


uploads = flask.Blueprint('uploads', __name__)


def get_extension(filename):
    if '.' in filename:
        basename, extension = filename.rsplit('.', 1)
        return basename, extension.lower()
    else:
        return None


@uploads.route('/<media_id>/subtitles', methods=['POST'])
@admin_required
def upload_subtitles(media_id):
    media_file = flask.request.files.get('media_file')
    if media_file is None or media_file.filename == '':
        return 'Bad request', 400

    filename = secure_filename(media_file.filename)
    basename, extension = get_extension(filename)

    if extension not in constants.SUB_EXTENSIONS:
        return 'Bad request', 400

    with tables.get_connection() as connection:
        results = connection.execute(
            tables.uploaded_media.select()
            .where(tables.uploaded_media.c.id == media_id)
            .where(tables.uploaded_media.c.type == 'video')
        )

        media_detail = results.first()

    if media_detail is None:
        return 'Not found', 404

    media_file.save(
        os.path.join(constants.VIDEOS_FOLDER, media_id, 'subs.vtt')
    )

    with tables.get_connection() as connection:
        connection.execute(
            tables.uploaded_media.update()
            .where(tables.uploaded_media.c.id == media_id)
            .values(subtitles=f'{constants.VIDEOS_ROOT}/{media_id}/subs.vtt')
        )

    return 'Created', 201


@uploads.route('/', methods=['POST'])
@admin_required
def upload_file():
    media_file = flask.request.files.get('media_file')
    if media_file is None or media_file.filename == '':
        return 'Bad request', 400

    filename = secure_filename(media_file.filename)
    basename, extension = get_extension(filename)
    last_modified_iso = flask.request.form.get('last_modified', None)

    is_image = extension in constants.IMAGE_EXTENSIONS
    is_video = extension in constants.VIDEO_EXTENSIONS
    not_media = not (is_image or is_video)
    bad_filename = filename == ''

    if not_media or bad_filename:
        return 'Bad request', 400

    media_id = uuid.uuid4()
    folder = constants.IMAGES_FOLDER if is_image else constants.VIDEOS_FOLDER
    media_dir = os.path.join(folder, str(media_id))
    os.makedirs(media_dir, exist_ok=True)

    unmodified = os.path.join(media_dir, f'unmodified.{extension}')
    original_file = os.path.join(media_dir, filename)
    small = os.path.join(media_dir, f'small.{extension}')
    xsmall = os.path.join(media_dir, f'xsmall.{extension}')

    media_file.save(unmodified)

    now = datetime.now()

    if is_image:
        save_rotated_image(unmodified, original_file)
        save_rotated_image(unmodified, small, 200)
        save_rotated_image(unmodified, xsmall, 50)

        if last_modified_iso is None:
            taken_default = now
        else:
            taken_default = dateutil.parser.parse(last_modified_iso)

        prefix = f'{constants.IMAGES_ROOT}/{media_id}'
        with tables.get_connection() as connection:
            connection.execute(tables.uploaded_media.insert().values(
                source=f'{prefix}/{filename}',
                small=f'{prefix}/small.{extension}',
                xsmall=f'{prefix}/xsmall.{extension}',
                type='image',
                name=filename,
                id=media_id,
                taken=get_image_datetime(media_file, default=taken_default),
                uploaded=now,
            ))

    if is_video:
        with tables.get_connection() as connection:
            connection.execute(tables.uploaded_media.insert().values(
                type='video',
                name=basename,
                id=media_id,
                uploaded=now,
                processed=False,
            ))

        transcode_video(media_id, basename, extension)

    return 'success', 201
