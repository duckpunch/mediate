import json

import flask
import requests
from authlib.client import OAuth2Session

from mediate.auth import (
    OIDC_SESSION,
    check_config_authorization,
    check_authorization,
    get_session_name,
)
from mediate.config import get_config


oidc = flask.Blueprint('oidc', __name__)


@oidc.route('/providers/<provider_key>')
def login(provider_key):
    provider = get_config()['oidc-providers'][provider_key]
    oauth_session = OAuth2Session(
        provider['key'],
        provider['secret'],
        scope=provider['scope'],
        redirect_uri=flask.request.host_url + 'api/oidc/callback',
    )

    uri, state = oauth_session.authorization_url(provider['authorize'])

    flask.session['oauth_state'] = (provider_key, state)

    return flask.redirect(uri)


@oidc.route('/callback')
def callback():
    provider_key, state = flask.session['oauth_state']
    provider = get_config()['oidc-providers'][provider_key]
    oauth_session = OAuth2Session(
        provider['key'],
        provider['secret'],
        state=state,
        redirect_uri=flask.request.host_url + 'api/oidc/callback',
    )
    token_response = oauth_session.fetch_access_token(
        provider['token'],
        code=flask.request.args['code'],
        method='POST',
    )

    headers = {'authorization': 'Bearer ' + token_response['access_token']}
    userinfo = requests.get(provider['userinfo'], headers=headers).json()

    flask.session[OIDC_SESSION] = json.dumps({
        'provider': provider_key,
        'userinfo': userinfo,
    })
    del flask.session['oauth_state']

    return flask.redirect('/')


@oidc.route('/session')
def auth_request_session():
    if OIDC_SESSION in flask.session:
        oidc_session = json.loads(flask.session[OIDC_SESSION])
        if check_authorization(oidc_session):
            is_admin = check_config_authorization(oidc_session)
            return flask.jsonify({
                'name': get_session_name(oidc_session),
                'role': 'admin' if is_admin else 'user',
            })
        else:
            del flask.session[OIDC_SESSION]

    return flask.jsonify({}), 401


@oidc.route('/logout')
def logout():
    if OIDC_SESSION in flask.session:
        del flask.session[OIDC_SESSION]
    response = flask.make_response(flask.jsonify({}))
    response.delete_cookie(OIDC_SESSION)
    return response
