import flask

from mediate.config import get_config


providers = flask.Blueprint('providers', __name__)


@providers.route('/')
def get_providers():
    oidc_providers = get_config()['oidc-providers']
    return flask.jsonify([
        dict(
            provider=provider,
            userinfo_key=config['userinfo-key'],
            label=config['label'],
        )
        for provider, config in oidc_providers.items()
    ])
