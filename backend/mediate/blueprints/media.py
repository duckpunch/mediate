import os
import shutil

import flask
import sqlalchemy

from mediate import constants
from mediate.auth import admin_required, auth_required
from mediate.persistence import tables


media = flask.Blueprint('media', __name__)


@media.route('/<media_id>')
@auth_required
def media_details(media_id):
    with tables.get_connection() as connection:
        results = connection.execute(tables.uploaded_media.select().where(
            tables.uploaded_media.c.id == media_id
        ))

        media_detail = results.first()
        if media_detail is None:
            return '', 404

        next_media = connection.execute(tables.uploaded_media.select().where(
            tables.uploaded_media.c.taken > media_detail.taken
        ).order_by(tables.uploaded_media.c.taken)).first()
        next_media_id = next_media.id if next_media else None

        prev_media = connection.execute(tables.uploaded_media.select().where(
            tables.uploaded_media.c.taken < media_detail.taken
        ).order_by(sqlalchemy.desc(tables.uploaded_media.c.taken))).first()
        prev_media_id = prev_media.id if prev_media else None

    return flask.jsonify(dict(
        **media_detail,
        next_media_id=next_media_id,
        prev_media_id=prev_media_id,
    ))


@media.route('/<media_id>', methods=['DELETE'])
@admin_required
def delete_media(media_id):
    with tables.get_connection() as connection:
        results = connection.execute(tables.uploaded_media.select().where(
            tables.uploaded_media.c.id == media_id
        ))

        media_detail = results.first()
        media_type = media_detail['type']
        if media_type == 'image':
            folder = constants.IMAGES_FOLDER
        else:
            folder = constants.VIDEOS_FOLDER

        media_dir = os.path.join(folder, media_id)

        connection.execute(
            tables.uploaded_media.delete().where(
                tables.uploaded_media.c.id == media_id,
            )
        )

        shutil.rmtree(media_dir)
    return ''


@media.route('/')
@auth_required
def get_media():
    with tables.get_connection() as connection:
        results = connection.execute(
            tables.uploaded_media.select()
            .where(tables.uploaded_media.c.processed == True)
            .order_by(tables.uploaded_media.c.taken)
        )

    return flask.jsonify([
        {
            'type': row['type'],
            'source': row['source'],
            'small': row['small'],
            'xsmall': row['xsmall'],
            'subtitles': row['subtitles'],
            'taken': row['taken'].strftime('%Y-%m-%d %H:%M:%S'),
            'id': row['id'],
            'name': row['name'],
        }
        for row in map(lambda raw: dict(raw.items()), results)
    ])
