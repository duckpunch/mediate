import sys

import piexif
from PIL import Image

from datetime import datetime


def get_exif(image):
    raw_exif = image.info.get('exif', None)
    return None if raw_exif is None else piexif.load(raw_exif)


def get_image_datetime(image_handle, default):
    image = Image.open(image_handle)
    exif = get_exif(image)
    if exif is None:
        return default
    else:
        try:
            timestamp = exif.get('0th', {}).get(piexif.ImageIFD.DateTime, None)
            return datetime.strptime(
                timestamp.decode('utf-8'),
                '%Y:%m:%d %H:%M:%S',
            ) if timestamp is not None else default
        except ValueError:
            return default


def save_rotated_image(original, destination, size=None):
    image = Image.open(original)

    exif = get_exif(image)
    if exif is not None:
        orientation = exif.get('0th', {}).get(piexif.ImageIFD.Orientation, 1)
    else:
        orientation = 1

    rotated_image = {
        1: lambda img: img,
        2: lambda img: img.transpose(Image.FLIP_LEFT_RIGHT),
        3: lambda img: img.transpose(Image.ROTATE_180),
        4: lambda img:
            img.transpose(Image.ROTATE_180).transpose(Image.FLIP_LEFT_RIGHT),
        5: lambda img:
            img.transpose(Image.ROTATE_270).transpose(Image.FLIP_LEFT_RIGHT),
        6: lambda img: img.transpose(Image.ROTATE_270),
        7: lambda img:
            img.transpose(Image.ROTATE_90).transpose(Image.FLIP_LEFT_RIGHT),
        8: lambda img: img.transpose(Image.ROTATE_90),
    }[orientation](image)

    if size is not None:
        thumbnail_size = sys.maxsize, size
        rotated_image.thumbnail(thumbnail_size)

    rotated_image.save(destination)
