import functools
import json

import flask
import sqlalchemy

from mediate import errors
from mediate.config import get_config
from mediate.persistence import tables


OIDC_SESSION = 'oidc-session'


def admin_required(func):
    @functools.wraps(func)
    def _admin_required(*args, **kwargs):
        if OIDC_SESSION in flask.session:
            oidc_session = json.loads(flask.session[OIDC_SESSION])
            if check_config_authorization(oidc_session):
                return func(*args, **kwargs)
        raise errors.Unauthorized()
    return _admin_required


def auth_required(func):
    @functools.wraps(func)
    def _auth_required(*args, **kwargs):
        if OIDC_SESSION in flask.session:
            oidc_session = json.loads(flask.session[OIDC_SESSION])
            if check_authorization(oidc_session):
                return func(*args, **kwargs)
        raise errors.Unauthorized()
    return _auth_required


def get_session_name(oidc_session):
    provider = oidc_session['provider']
    userinfo = oidc_session['userinfo']

    userinfo_key = get_config()['oidc-providers'][provider]['userinfo-key']
    return userinfo.get(userinfo_key)


def check_authorization(oidc_session):
    config_authorized = check_config_authorization(oidc_session)
    db_authorized = check_db_authorization(oidc_session)
    return config_authorized or db_authorized


def check_config_authorization(oidc_session):
    userinfo_value = get_session_name(oidc_session)

    return any(
        admin['userinfo-value'] == userinfo_value
        for admin in get_config()['admins']
    ) if userinfo_value is not None else False


def check_db_authorization(oidc_session):
    provider = oidc_session['provider']
    userinfo_value = get_session_name(oidc_session)
    active_state = True

    with tables.get_connection() as connection:
        current_user = tables.authorized_users.select().where(
            sqlalchemy.and_(
                tables.authorized_users.c.provider == provider,
                tables.authorized_users.c.userinfo_value == userinfo_value,
                tables.authorized_users.c.active == active_state,
            )
        )
        results = connection.execute(current_user)

    return bool(results.rowcount)
