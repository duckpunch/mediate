import os

import sqlalchemy


metadata = sqlalchemy.MetaData()


uploaded_media = sqlalchemy.Table(
    'uploaded_media', metadata,
    sqlalchemy.Column('id', sqlalchemy.Unicode, primary_key=True),
    sqlalchemy.Column('type', sqlalchemy.Unicode),
    sqlalchemy.Column('small', sqlalchemy.Unicode),
    sqlalchemy.Column('xsmall', sqlalchemy.Unicode),
    sqlalchemy.Column('source', sqlalchemy.Unicode),
    sqlalchemy.Column('subtitles', sqlalchemy.Unicode),
    sqlalchemy.Column('name', sqlalchemy.Unicode),
    sqlalchemy.Column('frame_count', sqlalchemy.Integer),
    sqlalchemy.Column(
        'uploaded',
        sqlalchemy.DateTime,
        server_default=sqlalchemy.sql.func.now(),
    ),
    sqlalchemy.Column(
        'taken',
        sqlalchemy.DateTime,
        server_default=sqlalchemy.sql.func.now(),
    ),
    sqlalchemy.Column(
        'processed',
        sqlalchemy.Boolean,
        nullable=False,
        server_default=sqlalchemy.text('true'),
    ),
)

authorized_users = sqlalchemy.Table(
    'authorized_users', metadata,
    sqlalchemy.Column('id', sqlalchemy.Unicode, primary_key=True),
    sqlalchemy.Column('userinfo_value', sqlalchemy.Unicode),
    sqlalchemy.Column('provider', sqlalchemy.Unicode),
    sqlalchemy.Column(
        'active',
        sqlalchemy.Boolean,
        nullable=False,
        server_default=sqlalchemy.text('true'),
    ),
)


def get_connection():
    engine = sqlalchemy.create_engine(os.environ['DATABASE_URL'])
    return engine.begin()
