"""add small/xsmall

Revision ID: a07642309b58
Revises: f8c5a306c7fa
Create Date: 2018-06-14 05:36:49.376060

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a07642309b58'
down_revision = 'f8c5a306c7fa'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('uploaded_media', sa.Column('small', sa.Unicode(), nullable=True))
    op.add_column('uploaded_media', sa.Column('xsmall', sa.Unicode(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('uploaded_media', 'xsmall')
    op.drop_column('uploaded_media', 'small')
    # ### end Alembic commands ###
