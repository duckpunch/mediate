import os
import subprocess
from datetime import datetime

import dateutil.parser
import redis
import rq

from mediate import constants
from mediate.images import save_rotated_image
from mediate.persistence import tables


connection = redis.from_url(os.getenv('REDIS_URL'))
queue = rq.Queue(connection=connection)


def _transcode_video(media_id, basename, extension):
    media_dir = os.path.join(constants.VIDEOS_FOLDER, str(media_id))

    original_file = f'{media_dir}/unmodified.{extension}'
    transcoded_file = f'{media_dir}/transcoded.mp4'
    screenshot_file = f'{media_dir}/screenshot.jpg'

    transcoding = subprocess.run([
        'ffmpeg', '-i', original_file, '-vcodec', 'libx264', '-acodec',
        'aac', transcoded_file,
    ])

    screenshotting = subprocess.run([
        'ffmpeg', '-i', original_file, '-vcodec', 'mjpeg', '-vframes', '1',
        '-f', 'image2', screenshot_file,
    ])

    successful_transcoding = (
        transcoding.returncode == 0 and screenshotting.returncode == 0
    )

    if successful_transcoding:
        small = os.path.join(media_dir, f'small.jpg')
        xsmall = os.path.join(media_dir, f'xsmall.jpg')
        save_rotated_image(screenshot_file, small, 200)
        save_rotated_image(screenshot_file, xsmall, 50)

        now = datetime.now()
        raw_creation = subprocess.check_output(
            f'ffprobe -i {original_file} -v error -show_entries '
            'stream_tags=creation_time -of default=noprint_wrappers=1:nokey=1',
            shell=True,
        ).decode('utf-8').strip().split('\n')

        if raw_creation:
            taken = dateutil.parser.parse(raw_creation[0])
        else:
            taken = now

        prefix = f'{constants.VIDEOS_ROOT}/{media_id}'
        with tables.get_connection() as connection:
            connection.execute(
                tables.uploaded_media.update()
                .where(tables.uploaded_media.c.id == str(media_id))
                .values(
                    source=f'{prefix}/transcoded.mp4',
                    small=f'{prefix}/small.jpg',
                    xsmall=f'{prefix}/xsmall.jpg',
                    processed=True,
                    taken=taken,
                )
            )


def transcode_video(media_id, basename, extension):
    return queue.enqueue_call(
        func=_transcode_video,
        args=(media_id, basename, extension),
        timeout='24h',
    )
