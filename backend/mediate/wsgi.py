import os

import flask

from mediate import errors
from mediate.blueprints.oidc import oidc
from mediate.blueprints.providers import providers
from mediate.blueprints.users import users
from mediate.blueprints.media import media
from mediate.blueprints.uploads import uploads
from mediate.constants import IMAGES_FOLDER, VIDEOS_FOLDER


os.makedirs(IMAGES_FOLDER, exist_ok=True)
os.makedirs(VIDEOS_FOLDER, exist_ok=True)


def disable_caching(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-invalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 0
    return response


application = flask.Flask(__name__)
application.secret_key = os.environ['FLASK_SECRET_KEY']

application.register_blueprint(media, url_prefix='/api/media')
application.register_blueprint(oidc, url_prefix='/api/oidc')
application.register_blueprint(providers, url_prefix='/api/providers')
application.register_blueprint(uploads, url_prefix='/api/uploads')
application.register_blueprint(users, url_prefix='/api/users')

application.after_request(disable_caching)


@application.errorhandler(errors.Unauthorized)
def unauthorized(error):
    return '', 401
