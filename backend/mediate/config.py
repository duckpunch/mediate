import operator

import voluptuous
from ruamel.yaml import YAML


_config = None


_base_schema = voluptuous.Schema({
    'oidc-providers': voluptuous.Schema({
        str: voluptuous.Schema({}, extra=voluptuous.ALLOW_EXTRA),
    }),

    'admins': voluptuous.Schema([
        voluptuous.Schema({
            'provider': str,
            'userinfo-value': str,
        })
    ]),
})


def validate_config(config):
    cleaned_config = _base_schema(config)

    admin_providers = set(map(
        operator.itemgetter('provider'),
        cleaned_config['admins'],
    ))
    all_providers = set(cleaned_config['oidc-providers'])

    if admin_providers <= all_providers:
        return cleaned_config
    else:
        raise Exception('Some admins have no providers in config')


def get_config():
    global _config

    if _config is None:
        _config = validate_config(YAML().load(open('/etc/mediate.yaml')))

    return _config
