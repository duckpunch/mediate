import os


IMAGES = 'images'
VIDEOS = 'videos'

UPLOAD_FOLDER = '/var/mediate'
IMAGES_ROOT = f'/media/{IMAGES}'
IMAGES_FOLDER = os.path.join(UPLOAD_FOLDER, IMAGES)
VIDEOS_ROOT = f'/media/{VIDEOS}'
VIDEOS_FOLDER = os.path.join(UPLOAD_FOLDER, VIDEOS)
IMAGE_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
VIDEO_EXTENSIONS = {'mov', 'mp4'}
SUB_EXTENSIONS = {'vtt'}
