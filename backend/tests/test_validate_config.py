import pytest

from mediate import config as module


@pytest.mark.parametrize(
    'config, should_raise',
    (
        (
            {
                'oidc-providers': {'gotlob': {}, 'gooolo': {}},
                'admins': [
                    {'provider': 'gotlob', 'userinfo-value': ''},
                ],
            },
            False,
        ),
        (
            {
                'oidc-providers': {'gotlob': {}, 'gooolo': {}},
                'admins': [
                    {'provider': 'foobooks', 'userinfo-value': ''},
                ],
            },
            True,
        ),
        (
            {
                'oidc-providers': {'gotlob': {}, 'gooolo': {}},
                'admins': [
                    {'provider': 'gotlob', 'userinfo-value': ''},
                    {'provider': 'gooolo', 'userinfo-value': ''},
                    {'provider': 'gotlob', 'userinfo-value': ''},
                ],
            },
            False,
        ),
        (
            {
                'oidc-providers': {'gotlob': {}, 'gooolo': {}},
                'admins': [
                    {'provider': 'gotlob', 'userinfo-value': ''},
                    {'provider': 'gooolo', 'userinfo-value': ''},
                    {'provider': 'foobooks', 'userinfo-value': ''},
                    {'provider': 'gotlob', 'userinfo-value': ''},
                ],
            },
            True,
        ),
    ),
)
def test_authorization(config, should_raise):
    if (should_raise):
        with pytest.raises(Exception):
            assert module.validate_config(config)
    else:
        assert module.validate_config(config)
