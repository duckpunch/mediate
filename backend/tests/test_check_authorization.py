import pytest

from mediate import auth as module


@pytest.mark.parametrize(
    'oidc_session, authorized',
    (
        ({'userinfo': {'profile': 'bar'}, 'provider': 'gitlobs'}, True),
        ({'userinfo': {'profile': 'bar'}, 'provider': 'foobook'}, False),
        ({'userinfo': {'profile': 'balls'}, 'provider': 'gitlobs'}, False),
        ({'userinfo': {'name': 'bar'}, 'provider': 'foobook'}, True),
    ),
)
def test_check_config_authorization(mocker, oidc_session, authorized):
    mocker.patch.object(
        module, 'get_config',
        return_value={
            'oidc-providers': {
                'foobook': {'userinfo-key': 'name'},
                'gitlobs': {'userinfo-key': 'profile'},
            },
            'admins': [
                {'provider': 'foobook', 'userinfo-value': 'bar'},
                {'provider': 'gitlobs', 'userinfo-value': 'beep'},
            ],
        },
    )

    assert module.check_config_authorization(oidc_session) == authorized
