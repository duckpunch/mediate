from setuptools import find_packages, setup

setup(
    name='mediate',
    version='0.1',
    packages=find_packages(exclude=('tests', 'docs')),
    include_package_data=True,
    install_requires=(
        'Authlib[crypto]',
        'Flask',
        'Pillow',
        'alembic',
        'piexif',
        'psycopg2-binary',
        'python-dateutil',
        'redis',
        'requests',
        'rq',
        'ruamel.yaml',
        'sqlalchemy',
        'voluptuous',
    ),
)
