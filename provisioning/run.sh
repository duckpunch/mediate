#!/bin/bash

set -e

cd /app && pip install -e .
chown mediate.www-data /app
chown mediate.www-data /var/mediate
cd /app/mediate/persistence && alembic upgrade head

uwsgi --uid mediate --gid www-data --master --workers 3 /etc/uwsgi.yaml \
& python /app/mediate/worker.py \
& /usr/sbin/nginx -g 'daemon off; master_process on;'
