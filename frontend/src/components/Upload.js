import React from 'react';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Dropzone from 'react-dropzone';
import concat from 'lodash/concat';
import reverse from 'lodash/reverse';

import {POST} from '../backend';


class UploadItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            percent: 0,
            hide: false,
        };

        const formData = new FormData();
        formData.append('media_file', props.file);
        if (props.file.lastModifiedDate) {
            formData.append('last_modified', props.file.lastModifiedDate.toISOString());
        }
        POST(
            '/api/uploads/',
            formData,
            this.onComplete.bind(this),
            this.onProgress.bind(this),
        );
    }

    onProgress(event) {
        this.setState({
            percent: event.hasOwnProperty('percent') ? event.percent : 0,
        });
    }

    onComplete() {
        this.setState({
            percent: 100,
        });
        setTimeout(() => {
            this.setState({
                hide: true,
            });
        }, 1000);
    }

    render() {
        if (this.state.hide) {
            return null;
        }

        const progress = {
            width: this.state.percent + '%',
        };
        return <div className='upload-item'>
            {this.props.file.name}
            <div className='upload-bar'>
                <div style={progress}></div>
            </div>
        </div>;
    }
}


export default class Upload extends React.Component {
    constructor() {
        super();

        this.state = {
            uploadQueue: [],
        };
    }

    onDrop(files) {
        this.setState({
            uploadQueue: concat(this.state.uploadQueue, files),
        });
    }

    render() {
        return <Container>
            <Dropzone onDrop={this.onDrop.bind(this)} className='dropzone' activeClassName='dropzone-active'>
                <div className='upload-message'>
                    <Button color='primary'>Select media</Button><br/>
                    Or drag &amp; drop anywhere on the box
                </div>
            </Dropzone>
            <h4>Current uploads</h4>
            {reverse(this.state.uploadQueue.map((file, index) =>
                <UploadItem file={file} key={index}/>
            ))}
        </Container>;
    }
}
