import React from 'react';
import {withRouter} from 'react-router-dom';
import AppBar from 'muicss/lib/react/appbar';
import {connect} from 'react-redux';

import Icon from './Icon';
import {GET} from '../backend';


class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
        };
    }

    onLogout() {
        this.hideMenu();
        const dispatch = this.props.dispatch;
        dispatch({
            type: 'sessionChange',
            session: null,
        });

        GET('/api/oidc/logout', () => {
            GET('/api/oidc/session', session => {
                dispatch({
                    type: 'sessionChange',
                    session,
                })
            })
        });
    }

    showMenu() {
        this.setState({showMenu: true});
    }

    hideMenu() {
        this.setState({showMenu: false});
    }

    navigateTo(destination) {
        return () => {
            this.hideMenu();
            this.props.history.push(destination);
        };
    }

    render() {
        const rotate = {
            transform: 'rotate(90deg)',
        };
        const hand = {
            cursor: 'pointer',
        };
        const isAdmin = this.props.session && this.props.session.role === 'admin';
        return <div>
            <AppBar>
                {this.state.showMenu &&
                    <div className='modal' onClick={this.hideMenu.bind(this)}></div>}
                {this.state.showMenu &&
                    <div className='menu'>
                        <AppBar>
                            <div className='mui--appbar-line-height app-bar-wrap'>
                                <span style={hand} onClick={this.hideMenu.bind(this)}>
                                    <Icon name='menu' />
                                </span>
                            </div>
                        </AppBar>
                        <div className='menu-link' onClick={this.navigateTo('/').bind(this)}>
                            <Icon style={rotate} name='dashboard' /> Home
                        </div>
                        {isAdmin &&
                            <div className='menu-link' onClick={this.navigateTo('/upload').bind(this)}>
                                <Icon name='add_a_photo' /> Upload
                            </div>}
                        {isAdmin &&
                            <div className='menu-link' onClick={this.navigateTo('/users').bind(this)}>
                                <Icon name='people' /> Users
                            </div>}
                        <div className='logout-link' onClick={this.onLogout.bind(this)}>
                            <Icon name='exit_to_app' /> Logout
                        </div>
                    </div>}
                <div className='mui--appbar-line-height app-bar-wrap'>
                    <span style={hand} onClick={this.showMenu.bind(this)}>
                        <Icon name='menu' />
                    </span>
                </div>
            </AppBar>
            <div className='disclaimer'>
                Hello! This website is still very much a work in progress. Let
                Carine or Frank know if you notice something broken!
            </div>
        </div>;
    }
}


export default withRouter(connect(
    state => ({session: state.session}),
    dispatch => ({dispatch}),
)(Navigation));
