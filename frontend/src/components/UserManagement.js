import React from 'react';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Divider from 'muicss/lib/react/divider';
import Select from 'muicss/lib/react/select';
import Option from 'muicss/lib/react/option';
import Input from 'muicss/lib/react/input';
import Panel from 'muicss/lib/react/panel';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import {connect} from 'react-redux';

import Icon from './Icon';
import {GET, POST, DELETE} from '../backend';
import store from '../store';


function refreshUsers() {
    GET('/api/users', users => {
        store.dispatch({
            type: 'usersChange',
            users,
        });
    });
}


class SingleUser extends React.Component {
    onDeleteClick() {
        DELETE('/api/users/' + this.props.id, refreshUsers);
    }

    render() {
        const hand = {
            cursor: 'pointer',
        };
        return <Row key={this.props.id}>
            <Col md='6'>{this.props.userinfo_value}</Col>
            <Col md='5'>{this.props.provider}</Col>
            <Col md='1'><span onClick={this.onDeleteClick.bind(this)} style={hand}>
                <Icon name='delete_forever' />
            </span></Col>
        </Row>;
    }
}


class _ProviderSelection extends React.Component {
    componentWillMount() {
        if (this.props.providers && this.props.onProviderChange) {
            this.props.onProviderChange(
                this.props.providers[0].provider,
                this.props.providers[0].userinfo_key,
            );
        }
    }

    onChange(event) {
        const newProvider = event.target.value;
        const userinfoKey = this.props.providers.find(
            provider => provider.provider === newProvider
        ).userinfo_key;

        if (this.props.onProviderChange) {
            this.props.onProviderChange(newProvider, userinfoKey);
        }
    }

    render() {
        return <Select label='Provider' onChange={this.onChange.bind(this)}>
            {this.props.providers && this.props.providers.map(provider =>
                <Option key={provider.provider}
                    label={provider.label} value={provider.provider} />
            )}
        </Select>;
    }
}


const ProviderSelection = connect(
    state => ({providers: state.providers}),
)(_ProviderSelection);


class NewUser extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            provider: '',
            userinfoKey: '',
        };

        this.onNameChange = this.onNameChange.bind(this);
        this.onProviderChange = this.onProviderChange.bind(this);
        this.onCreateUser = this.onCreateUser.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    onNameChange(event) {
        this.setState({
            name: event.target.value,
        });
    }

    onProviderChange(provider, userinfoKey) {
        const titleUserInfoKey = userinfoKey.charAt(0).toUpperCase() + userinfoKey.slice(1);
        this.setState({provider, userinfoKey: titleUserInfoKey});
    }

    onCreateUser() {
        if (this.props.onCreateUser) {
            this.props.onCreateUser(this.state.provider, this.state.name);
        }
    }

    onCancel() {
        if (this.props.onCancel) {
            this.props.onCancel();
        }
    }

    render() {
        return <Panel>
            <Input floatingLabel label={this.state.userinfoKey} onChange={this.onNameChange} />
            <ProviderSelection onProviderChange={this.onProviderChange} />
            <Button color='primary' onClick={this.onCreateUser}>Add User</Button>
            <Button color='primary' variant='flat' onClick={this.onCancel}>Cancel</Button>
        </Panel>;
    }
}


class UserManagement extends React.Component {
    constructor() {
        super();
        this.state = {};

        this.onAddUser = this.onAddUser.bind(this);
        this.onCreateUser = this.onCreateUser.bind(this);
        this.onCancel = this.onCancel.bind(this);

        refreshUsers();
    }

    onAddUser() {
        this.setState({
            addUser: true,
        });
    }

    onCancel() {
        this.setState({
            addUser: false,
        });
    }

    onCreateUser(provider, userinfo_value) {
        POST(
            '/api/users/',
            JSON.stringify({provider, userinfo_value}),
            refreshUsers,
        );

        this.setState({
            addUser: false,
        });
    }

    render() {
        return <Container>
            {!this.state.addUser && <Button color='primary' onClick={this.onAddUser}>Add new user</Button>}
            {this.state.addUser && <NewUser onCreateUser={this.onCreateUser} onCancel={this.onCancel}/>}
            <Row>
                <Col md='6' className='mui--text-menu'>Key</Col>
                <Col md='5' className='mui--text-menu'>Provider</Col>
            </Row>
            <Divider />
            {this.props.users && this.props.users.map(
                userProps => <SingleUser key={userProps.id} {...userProps} />
            )}
        </Container>;
    }
}


export default connect(
    state => ({users: state.users}),
)(UserManagement);
