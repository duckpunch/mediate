import React from 'react';
import {Link} from 'react-router-dom';
import Container from 'muicss/lib/react/container';
import groupBy from 'lodash/groupBy';
import reverse from  'lodash/reverse';
import sortBy from 'lodash/sortBy';
import toPairs from 'lodash/toPairs';

import {GET} from '../backend';
import BackgroundedImage from './BackgroundedImage';


function Media(props) {
    const source = props.small;
    return <BackgroundedImage source={source} className='img-box' height={200} scaleDown>
        <Link to={'/m/' + props.id}></Link>
    </BackgroundedImage>;
}


class Group extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMembers: false,
        };
    }

    toggleMembers() {
        this.setState({
            showMembers: !this.state.showMembers,
        });
    }

    render() {
        const [year, month] = this.props.slug.split('-');
        const prettyMonth = [
            'January', 'February', 'March', 'April', 'May', 'June', 'July',
            'August', 'September', 'October', 'November', 'December',
        ][parseInt(month, 10)];
        return <div onClick={this.toggleMembers.bind(this)} className='gallery-toggle'>
            <h4>{prettyMonth} {year}</h4>
            {this.state.showMembers && this.props.members.map(
                imgProps => <Media key={imgProps.id} {...imgProps} />
            )}
            <div onClick={this.toggleMembers.bind(this)} className='gallery-toggle'>
                {!this.state.showMembers && this.props.members.map(
                    imgProps => <BackgroundedImage backgroundSize='cover'
                        width={30} height={30}
                        source={imgProps.xsmall}
                        key={imgProps.id + '-xsmall'}/>
                )}
            </div>
        </div>;
    }
}


export default class Gallery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        GET(props.mediaUrl || '/api/media', this.onMediaLoaded.bind(this));
    }

    onMediaLoaded(members) {
        if (members) {
            this.setState({members});
        }
    }

    render() {
        const {members} = this.state;
        if (!members) return null;

        const grouped = groupBy(members, media => {
            const dateToken = media.taken.split(/[^0-9]/);
            const date = new Date(
                dateToken[0], dateToken[1] - 1, dateToken[2],
                dateToken[3], dateToken[4], dateToken[5],
            );
            const month = (date.getMonth() < 10 ? '0' : '') + date.getMonth();
            return date.getFullYear() + '-' + month;
        });
        const sortedGroups = reverse(sortBy(toPairs(grouped), [0]));

        return <Container fluid className='gallery'>
            {sortedGroups.map(
                ([key, groupMembers]) =>
                    <Group key={key} members={reverse(groupMembers)} slug={key} />
            )}
        </Container>;
    }
}
