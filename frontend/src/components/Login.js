import React from 'react';
import Container from 'muicss/lib/react/container';
import {connect} from 'react-redux';

import Icon from './Icon';


function LoginButton(props) {
    const commonWidth = {
        width: '200px',
    };
    return <div><a
        style={commonWidth}
        className='mui-btn mui-btn--primary'
        href={'/api/oidc/providers/' + props.provider}>
        <Icon name='lock'/>
        {props.provider}
    </a></div>;
}


function Login(props) {
    return <Container className='mui--text-center'>
        <h2>mediate</h2>
        {props.providers && props.providers.map(
            provider => <LoginButton {...provider}/>
        )}
    </Container>;
}


export default connect(
    state => ({providers: state.providers}),
)(Login);
