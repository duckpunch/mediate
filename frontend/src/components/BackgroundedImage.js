import React from 'react';


function isPhone() {
    const nonPhone = window.innerHeight > 850 || window.innerWidth > 850;
    return !nonPhone;
}


export default class BackgroundedImage extends React.Component {
    constructor(props) {
        super(props);

        const image = new Image();
        image.onload = this.onImageLoad.bind(this)();

        this.state = {
            isPhone: isPhone(),
            source: props.source,
            loadedImage: image,
        };

        image.src = this.state.source;

        window.addEventListener(
            'resize',
            this.onResize.bind(this),
        );
    }

    onResize() {
        this.setState({isPhone: isPhone()});
    }

    onImageLoad() {
        return () => {
            const image = this.state.loadedImage;
            this.setState({
                widthRatio: image.width / image.height,
                heightRatio: image.height / image.width,
                imageWidth: image.width,
                imageHeight: image.height,
            });
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.source !== prevState.source) {
            prevState.loadedImage.src = nextProps.source;
            return {source: nextProps.source};
        }
        return null;
    }

    render() {
        const imageStyle = {
            backgroundRepeat: 'no-repeat',
            backgroundSize: this.props.backgroundSize || 'contain',
            backgroundImage: `url(${this.state.source})`,
        };

        const factor = this.state.isPhone && this.props.scaleDown ? 0.4 : 1;
        const height = this.props.height ? this.props.height * factor : undefined;
        const width = this.props.width ? this.props.width * factor : undefined;

        if (height && width) {
            imageStyle['width'] = `${width}px`;
            imageStyle['height'] = `${height}px`;
        } else if (height && width === undefined) {
            imageStyle['width'] = `${this.state.widthRatio * height}px`;
            imageStyle['height'] = `${height}px`;
        } else if (width && height === undefined) {
            imageStyle['width'] = `${width}px`;
            imageStyle['height'] = `${this.state.heightRatio * width}px`;
        } else {
            imageStyle['width'] = `${this.state.imageWidth}px`;
            imageStyle['height'] = `${this.state.imageHeight}px`;
        }

        const otherClasses = this.props.className || '';

        return <div className={'backgrounded-image ' + otherClasses} style={imageStyle}>
            {this.props.children}
        </div>;
    }
}
