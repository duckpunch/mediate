import React from 'react';
import {withRouter} from 'react-router-dom';
import Container from 'muicss/lib/react/container';
import Button from 'muicss/lib/react/button';
import Dropzone from 'react-dropzone';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import head from 'lodash/head';
import {connect} from 'react-redux';

import {GET, DELETE, POST} from '../backend';
import BackgroundedImage from './BackgroundedImage';
import Icon from './Icon';


class Detail extends React.Component {
    constructor(props) {
        super();

        this.state = {
            details: {},
            pendingDeletion: false,
        };

        this.onDetailsLoaded = this.onDetailsLoaded.bind(this);
        this.onPendingDeletion = this.onPendingDeletion.bind(this);
        this.onCancelDeletion = this.onCancelDeletion.bind(this);
        this.onDelete = this.onDelete.bind(this);

        this.loadMedia(props.match.params.mediaId);
    }

    loadMedia(mediaId) {
        GET(`/api/media/${mediaId}`, this.onDetailsLoaded);
    }

    onDelete() {
        const mediaId = this.props.match.params.mediaId;
        DELETE('/api/media/' + mediaId, () => {
            this.props.history.push('/');
        });
    }

    onDetailsLoaded(details) {
        this.setState({details});
    }

    onPendingDeletion() {
        this.setState({pendingDeletion: true});
    }

    onCancelDeletion() {
        this.setState({pendingDeletion: false});
    }

    onComplete() {
        const mediaId = this.props.match.params.mediaId;
        this.loadMedia(mediaId);
    }

    onDrop(files) {
        const mediaId = this.props.match.params.mediaId;
        const file = head(files);
        const formData = new FormData();
        formData.append('media_file', file);

        POST('/api/uploads/' + mediaId + '/subtitles', formData, this.onComplete.bind(this));
    }

    onPrevious() {
        const newMediaId = this.state.details.prev_media_id;
        if (newMediaId) {
            this.props.history.push(`/m/${newMediaId}`);
            this.loadMedia(newMediaId);
        }
    }

    onNext() {
        const newMediaId = this.state.details.next_media_id;
        if (newMediaId) {
            this.props.history.push(`/m/${newMediaId}`);
            this.loadMedia(newMediaId);
        }
    }

    render() {
        const details = this.state.details;
        const detailNav = {
            verticalAlign: 'bottom',
            cursor: 'pointer',
        };

        return <Container><Row>
            <Col md='6' className='detail-box'>
                <div>
                    {details.next_media_id &&
                        <span onClick={this.onNext.bind(this)}>
                            <Icon style={detailNav} name='keyboard_arrow_left' />
                        </span>}
                    <div className='detail-name'>{details.name}</div>
                    {details.prev_media_id &&
                        <span onClick={this.onPrevious.bind(this)}>
                            <Icon style={detailNav} name='keyboard_arrow_right' />
                        </span>}
                </div>
                {details.type === 'image' &&
                    <BackgroundedImage
                        source={details.source}/>}
                {details.type === 'video' &&
                    <video controls controlsList='nodownload' src={details.source}>
                        {details.subtitles &&
                            <track label='English' kind='subtitles' srcLang='en'
                                src={details.subtitles} default />}
                    </video>}
            </Col>
            <Col md='6'>
                <div>Taken: {details.taken}</div>
                {this.props.session.role === 'admin' && details.type === 'video' ?
                    <Dropzone className='subs-dropzone' accept='.vtt' activeClassName='dropzone-active'
                        onDropAccepted={this.onDrop.bind(this)}>
                        <div className='upload-message'>Add subtitles</div>
                    </Dropzone>
                    : null}
                {this.props.session.role === 'admin' ? (this.state.pendingDeletion ?
                    <div>
                        <h5>Are you sure?</h5>
                        <Button color='danger' onClick={this.onDelete}>Delete</Button>
                        <Button variant='flat' onClick={this.onCancelDeletion}>Cancel</Button>
                    </div> :
                    <Button color='primary' onClick={this.onPendingDeletion}>
                        Delete
                    </Button>) : null}
            </Col>
        </Row></Container>;
    }
}


export default withRouter(connect(
    state => ({session: state.session}),
)(Detail));
