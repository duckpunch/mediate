import React from 'react';


export default function Icon(props) {
    return <i style={props.style} className='material-icons'>{props.name}</i>;
}
