import request from 'superagent';


function GET(url, callback) {
    return fetch(url, {credentials: 'same-origin'}).then(
        (response) => {
            response.json().then(callback).catch(callback);
        }
    ).catch(callback);
}


function POST(url, body, callback = () => {}, progress = () => {}) {
    return request
        .post(url)
        .withCredentials()
        .on('progress', progress)
        .send(body)
        .then(callback);
}


function DELETE(url, callback = () => {}) {
    return fetch(
        url, {
            method: 'DELETE',
            credentials: 'same-origin',
        },
    ).then(callback).catch(callback);
}


export default {GET, POST, DELETE};
