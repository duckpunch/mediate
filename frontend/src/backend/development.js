import range from 'lodash/range';

const nyan_cat = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f35',
    name: 'nyan-cat.png',
    source: '/static/public/img/nyan.png',
    small: '/static/public/img/nyan.png',
    xsmall: '/static/public/img/nyan.png',
    taken: '2016-06-30 14:44:41',
    type: 'image',
    next_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f36',
    prev_media_id: null,
};

const jazz_nyan = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f36',
    name: 'jazz-cat.png',
    source: '/static/public/img/jazznyan.jpg',
    small: '/static/public/img/jazznyan.jpg',
    xsmall: '/static/public/img/jazznyan.jpg',
    taken: '2016-07-20 14:44:41',
    type: 'image',
    prev_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f35',
    next_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f37',
};

const mexi_nyan = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f37',
    name: 'mexinyan.png',
    source: '/static/public/img/mexinyan.jpg',
    small: '/static/public/img/mexinyan.jpg',
    xsmall: '/static/public/img/mexinyan.jpg',
    taken: '2016-07-30 14:44:41',
    type: 'image',
    prev_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f36',
    next_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f38',
};

const tac_nayn = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f38',
    name: 'tacnayn.png',
    source: '/static/public/img/tacnayn.png',
    small: '/static/public/img/tacnayn.png',
    xsmall: '/static/public/img/tacnayn.png',
    taken: '2016-08-20 14:44:41',
    type: 'image',
    prev_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f37',
    next_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f39',
};

const nyan_dog = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f39',
    name: 'nyan-dog.png',
    source: '/static/public/img/nyandog.png',
    small: '/static/public/img/nyandog.png',
    xsmall: '/static/public/img/nyandog.png',
    taken: '2016-08-30 14:44:41',
    type: 'image',
    prev_media_id: '424d8910-1ad5-4039-a23c-cdea77bc9f38',
    next_media_id: null,
};

const test_video = {
    id: '424d8910-1ad5-4039-a23c-cdea77bc9f40',
    name: 'some_video',
    source: 'https://archive.org/download/BigBuckBunny_124/Content/big_buck_bunny_720p_surround.mp4',
    subtitles: '/static/public/subs/test.vtt',
    taken: '2016-08-30 14:44:41',
    type: 'video',
    prev_media_id: null,
    next_media_id: null,
}

const defaults = {
    '/api/users': [
        {id: '9f13c61d-b463-49c4-8783-bc890fb1feb6', userinfo_value: 'foo@bar.com', provider: 'googlo'},
        {id: '9f13c61d-b463-49c4-8783-bc890fb1feb7', userinfo_value: 'bar@foo.com', provider: 'googlo'},
        {id: '9f13c61d-b463-49c4-8783-bc890fb1feb8', userinfo_value: 'https://gotlob.com/dockponch', provider: 'gotlob'},
    ],
    '/api/providers': [
        {
            label: 'Gotlob',
            provider: 'gotlob',
            userinfo_key: 'profile',
        },
        {
            label: 'Googlo',
            provider: 'googlo',
            userinfo_key: 'email',
        }
    ],
    '/api/media': [nyan_cat, jazz_nyan, mexi_nyan, tac_nayn, nyan_dog],
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f35': nyan_cat,
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f36': jazz_nyan,
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f37': mexi_nyan,
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f38': tac_nayn,
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f39': nyan_dog,
    '/api/media/424d8910-1ad5-4039-a23c-cdea77bc9f40': test_video,
    '/api/oidc/session': {
        role: 'user',
        name: 'duckpunch',
    },
};


function GET(url, callback) {
    console.log('GET', url);
    const defaultValue = defaults[url] || null;

    setTimeout(() => {
        if (defaultValue === null) {
            console.log('GET: No value set for', url);
        }
        callback(defaultValue);
    }, 500);
}


function POST(url, body, callback = () => {}, progress = () => {}) {
    console.log('POST', url, body);
    const defaultValue = defaults[url] || null;

    range(10).map(n => {
        return setTimeout(
            () => {progress({
                percent: n * 10,
            });}
        , 500 * n);
    });

    setTimeout(() => {
        if (defaultValue === null) {
            console.log('POST: No value set for', url);
        }
        callback(defaultValue);
    }, 5000);
}


function DELETE(url, callback = () => {}) {
    console.log('DELETE', url);
    setTimeout(() => {
        callback();
    }, 500);
}


export default {GET, POST, DELETE};
