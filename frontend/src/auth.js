import React from 'react';
import {Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';


function _AuthorizedRoute(props) {
    const {component: Component, hideLoading, session, requiresAdmin, ...rest} = props;
    return <Route {...rest} render={router_props => {
        if (session === null) {
            if (hideLoading) {
                return null;
            } else {
                return <div>loading</div>;
            }
        } else {
            const loggedIn = Boolean(session.role);
            const correctRole = requiresAdmin ? session.role === 'admin' : true;
            if (loggedIn && correctRole) {
                return <Component {...router_props} />;
            } else {
                return <Redirect to='/login' />;
            }
        }
    }} />;
}


export const AuthorizedRoute = withRouter(connect(
    state => ({session: state.session}),
)(_AuthorizedRoute));


function _LoginRoute(props) {
    const {component: Component, showLoading, session, ...rest} = props;
    return <Route render={props => {
        if (session === null) {
            if (showLoading) {
                return <div>loading</div>;
            } else {
                return null;
            }
        } else {
            if (session.role) {
                return <Redirect to='/' />;
            } else {
                return <Component {...props} />;
            }
        }
    }} {...rest} />;
}


export const LoginRoute = withRouter(connect(
    state => ({session: state.session}),
)(_LoginRoute));
