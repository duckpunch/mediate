import {createStore} from 'redux'


const defaultState = {
    session: null,
    providers: [],
    users: [],
};


function rootReducer(state = defaultState, action) {
    if (action.type === 'sessionChange') {
        return {...state, session: action.session};
    }
    if (action.type === 'providersChange') {
        return {...state, providers: action.providers};
    }
    if (action.type === 'usersChange') {
        return {...state, users: action.users};
    }
    return state;
}


export default createStore(rootReducer);
