import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';

import Detail from './components/Detail';
import Gallery from './components/Gallery';
import Login from './components/Login';
import Navigation from './components/Navigation';
import Upload from './components/Upload';
import UserManagement from './components/UserManagement';
import store from './store';
import {AuthorizedRoute, LoginRoute} from './auth';
import {GET} from './backend';

import './App.css';


GET('/api/oidc/session', session => {
    store.dispatch({
        type: 'sessionChange',
        session,
    });
});


GET('/api/providers', providers => {
    store.dispatch({
        type: 'providersChange',
        providers,
    });
});


export default function App(props) {
    return <Provider store={store}>
        <BrowserRouter><div className='App'>
            <Switch>
                <LoginRoute exact path='/login' component={Login} />
                <Route component={Navigation} />
            </Switch>
            <AuthorizedRoute exact path='/' component={Gallery} />
            <AuthorizedRoute exact requiresAdmin path='/upload' component={Upload} />
            <AuthorizedRoute exact requiresAdmin path='/users' component={UserManagement} />
            <AuthorizedRoute path='/m/:mediaId' component={Detail} />
        </div></BrowserRouter>
    </Provider>;
}
