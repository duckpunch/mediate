FROM python:3.6.5-alpine3.7

ADD https://github.com/krallin/tini/releases/download/v0.17.0/tini-static /sbin/tini
RUN chmod +x /sbin/tini

WORKDIR /app
COPY backend/requirements.txt /app

RUN apk --no-cache add nginx bash postgresql-libs openjpeg jpeg zlib freetype \
    lcms2 tiff tk tcl ffmpeg

RUN apk --no-cache add --virtual .build-dependencies \
    g++ musl-dev linux-headers postgresql-dev libffi-dev jpeg-dev zlib-dev \
    freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev \
    && pip install uwsgi \
    && pip install -r requirements.txt \
    && apk del .build-dependencies


COPY frontend/build /frontend
COPY backend/ /app/
COPY provisioning/run.sh /sbin/run.sh
COPY provisioning/nginx.conf /etc/nginx/conf.d/mediate.conf
COPY provisioning/uwsgi.yaml /etc/uwsgi.yaml

RUN addgroup -S mediate \
    && adduser -S -G www-data -H -s /bin/bash www-data \
    && adduser -S -G mediate -G www-data -h /var/lib/mediate \
       -s /bin/bash mediate \
    && mkdir -p /var/mediate \
    && mkdir -p /run/nginx \
    && chown -R mediate.www-data /app \
    && chown -R mediate.www-data /var/mediate \
    && rm /etc/nginx/conf.d/default.conf \
    && chmod +x /sbin/run.sh

EXPOSE 80

ENTRYPOINT ["/sbin/tini", "-g", "--"]

CMD ["/sbin/run.sh"]